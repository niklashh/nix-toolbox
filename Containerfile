FROM docker.io/library/debian:bookworm

LABEL com.github.containers.toolbox="true" \
      usage="This image is meant to be used with the toolbox command" \
      maintainer="Niklas Halonen <niklas.2.halonen@aalto.fi>"

# update and install base packages
RUN apt update -y
RUN apt -y install --no-install-recommends \
    sudo \
    ca-certificates \
    psmisc \
    procps \
    less \
    xz-utils \
    vim-tiny \
    nano \
    curl \
    git \
    ssh \
    direnv \
    gnupg2 \
    iproute2 \
    inetutils-ping \
    rsync \
    lsb-release \
    dialog \
    locales \
    man-db \
    bash-completion \
    acl \
    libcap2-bin \
    libnss-myhostname \
    fish

# create at least locae for en_US.UTF-8
RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen && locale-gen

RUN sed -i -e 's/ ALL$/ NOPASSWD:ALL/' /etc/sudoers

RUN touch /etc/localtime
RUN echo VARIANT_ID=container >> /etc/os-release

# create non-root user and group and add it sudoers
ARG USERNAME=nix
ARG USER_UID=1000
ARG USER_GID=${USER_UID}
RUN groupadd --gid ${USER_GID} ${USERNAME} && \
    useradd --uid ${USER_UID} --gid ${USER_GID} -m ${USERNAME} -s /bin/bash && \
    echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/nix && \
    chmod 0440 /etc/sudoers.d/nix

# copy nix configuration
COPY nix.conf /etc/nix/nix.conf

# install nix
ARG NIX_INSTALL_SCRIPT=https://nixos.org/nix/install
# RUN bash -c 'sh <(curl -L ${NIX_INSTALL_SCRIPT}) --no-daemon'
RUN bash -c 'curl -L ${NIX_INSTALL_SCRIPT} | sudo -u ${USERNAME} NIX_INSTALLER_NO_MODIFY_PROFILE=1 sh'

# Destroy leftovers
RUN rm -rf /home && userdel -f ${USERNAME}