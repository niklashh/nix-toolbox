# Nix-toolbox

[![pipeline status](https://gitlab.com/niklashh/nix-toolbox/badges/master/pipeline.svg)](https://gitlab.com/niklashh/nix-toolbox/-/commits/master)

This container can be used for running nix inside [toolbox](https://github.com/containers/toolbox).

```console
toolbox create --image registry.gitlab.com/niklashh/nix-toolbox:latest
toolbox enter nix-toolbox
```

## Shell integration

You can configure your shell by runing the nix install script inside the toolbox. This modifies the shell configuration **on your host**.

```console
sh <(curl -L https://nixos.org/nix/install) --no-daemon
```

TODO other options?

## Building manually

To build the toolbox container

```console
./build.sh
```

To start the toolbox

```console
toolbox create --image localhost/nix-toolbox
toolbox enter nix-toolbox
```

To remove the toolbox

```console
toolbox rm -f nix-toolbox
```

## License

MIT
